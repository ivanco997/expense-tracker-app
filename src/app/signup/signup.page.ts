import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { UserFacade } from '@expenseapp/tabs/store/facade/user.facade';

@Component({
  selector: 'app-signup',
  templateUrl: './signup.page.html',
  styleUrls: ['./signup.page.scss'],
})
export class SignupPage implements OnInit {
  registrationForm: FormGroup;

  constructor(private fb: FormBuilder, private userFacade: UserFacade) { }

  ngOnInit(): void {
    this.initForm();
  }

  submit(): void {
    this.userFacade.register(this.registrationForm.value);
  }

  private initForm(): void {
    this.registrationForm = this.fb.group({
      firstName: ['', Validators.required],
      lastName: ['', Validators.required],
      username: ['', Validators.required],
      password: ['', Validators.required],
      amount: 0
    })
  }

}
