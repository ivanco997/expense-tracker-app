import {
  Component,
  ChangeDetectionStrategy,
  Output,
  EventEmitter
} from '@angular/core';
import { ICredentials } from '../../models/user.model';
import { NgForm } from '@angular/forms';

@Component({
  selector: 'app-login-form',
  templateUrl: './form.component.html',
  styleUrls: ['./form.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class FormComponent {
  @Output() submitted: EventEmitter<ICredentials> = new EventEmitter();

  onSubmit(form: NgForm): void {
    const credentials: ICredentials = form.value;
    this.submitted.emit(credentials);
  }
}
