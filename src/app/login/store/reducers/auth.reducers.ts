import { IUser } from '../../../models/user.model';
import { createReducer, on, Action } from '@ngrx/store';
import {
  loginSuccess,
  logoutAction,
  isLoggedInAction,
  isLoggedInSuccess,
  getUserData,
  getUserDataSuccess
} from '../actions/login.actions';
import * as jwtDecode from 'jwt-decode';
import { updateUserAmout, updateUserAmoutSuccess } from '../actions';

export const authStateFeatureKey = 'authFeatureKey';

export interface IAuthState {
  user: IUser | null;
  isLoggedIn?: boolean;
}

export const initialState: IAuthState = {
  user: null
};

export const reducer = createReducer(
  initialState,
  on(loginSuccess, (state, { response }) => ({ ...state, user: response.user, isLoggedIn: true })),
  on(logoutAction, () => initialState),
  on(isLoggedInAction, state => ({ ...state })),
  on(isLoggedInSuccess, (state, { token, isLoggedIn }) => ({
    ...state,
    isLoggedIn,
    user: jwtDecode<IUser>(token)
  })),
  on(getUserData, state => ({ ...state })),
  on(getUserDataSuccess, (state, { user }) => ({
    ...state,
    user
  })),
  on(updateUserAmout, state => ({ ...state })),
  on(updateUserAmoutSuccess, (state, { user }) => ({
    ...state,
    user
  }))
);

export function authReducer(state: IAuthState | undefined, action: Action) {
  return reducer(state, action);
}

export const getUser = (state: IAuthState) => state.user;
