import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable, of } from 'rxjs';
import { IUser, ICredentials, ILoginResponse } from '../../../models/user.model';
import { environment } from '../../../../environments/environment';
import * as jwtDecode from 'jwt-decode';

@Injectable({
  providedIn: 'root'
})
export class LoginService {
  constructor(private http: HttpClient) {}

  public login(credentials: ICredentials): Observable<ILoginResponse> {
    return this.http.post<ILoginResponse>(
      `${environment.API_URL}auth/login`,
      credentials
    );
  }

}
