import { createAction, props } from '@ngrx/store';
import { IUser } from '@expenseapp/models/user.model';

export const updateUserAmout = createAction(
    '[User] Update user amount',
    props<{ userId: string; amount: number }>()
)

export const updateUserAmoutSuccess = createAction(
    '[User] Update user amount success',
    props<{ user: IUser }>()
)

export const updateUserAmoutFailed = createAction(
    '[User] Update user amount failed',
    (error: Error) => ({ error })
)