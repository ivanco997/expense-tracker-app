import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { IonicModule } from '@ionic/angular';
import { LoginPage } from './page/login.page';
import { LoginPageRoutingModule } from './login.routing.module';
import { FormComponent } from './form/form.component';
import { EffectsModule } from '@ngrx/effects';
import { LoginEffects } from './store/effects/login.effects';
import { StoreModule } from '@ngrx/store';
import { userFeatureKey, loginAuthReducerMap } from './store/reducers';
import { AuthGuardService } from './store/guards/auth.guard';
import { SharedModule } from '@expenseapp/shared/shared.module';
import { UserEffects } from './store/effects/user.effects';
@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    LoginPageRoutingModule,
    StoreModule.forFeature(userFeatureKey, loginAuthReducerMap),
    EffectsModule.forFeature([LoginEffects, UserEffects]),
    SharedModule
  ],
  declarations: [LoginPage, FormComponent],
  providers: [AuthGuardService]
})
export class LoginPageModule {}
