import { Component, Input } from '@angular/core';
import { IUser } from '@expenseapp/models/user.model';

@Component({
  selector: 'app-sidebar-menu-details',
  templateUrl: './details.component.html',
  styleUrls: ['./details.component.scss'],
})
export class SidebarMenuDetailsComponent {
  @Input() user: IUser;
  public darkMode: boolean = JSON.parse(localStorage.getItem('darkMode'));

  constructor() {
    if (localStorage.getItem('darkMode')) {
      const mode = JSON.parse(localStorage.getItem('darkMode'));
      document.body.classList.toggle('dark', mode);
    }
  }

  public toggleChange(event: CustomEvent): void {
    document.body.classList.toggle('dark', event.detail.checked);
    localStorage.setItem('darkMode', event.detail.checked);
  }
}
