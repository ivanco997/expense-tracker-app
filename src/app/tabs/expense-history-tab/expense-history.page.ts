import { Component } from '@angular/core';
import { Observable } from 'rxjs';
import { IExpenseResponse } from '../../models/expense.model';
import { ExpenseFacade } from '../store/facade/expense.facade';

@Component({
  selector: 'app-expense-history',
  templateUrl: 'expense-history.page.html',
  styleUrls: ['expense-history.page.scss']
})
export class ExpenseHistoryPage {
  expenses$: Observable<IExpenseResponse> = this.expenseFacade.expenses$;
  isLoaded$: Observable<boolean> = this.expenseFacade.isLoaded$;
  hasError$: Observable<boolean> = this.expenseFacade.hasError$;

  public segment: string;
  public skip: number = 0;

  constructor(private expenseFacade: ExpenseFacade) { }

  ionViewWillEnter(): void {
    this.segment = 'today';
    this.expenseFacade.resetExpenseList();
    this.expenseFacade.getExpenseList(this.segment, this.skip, 10);
  }

  segmentChange(date: string): void {
    if (this.segment !== date) {
      this.segment = date;
      this.skip = 0;
      this.expenseFacade.resetExpenseList();
      this.expenseFacade.getExpenseList(date, this.skip, 10);
    }
  }

  skipItems(limit: number): void {
    this.skip += limit;
    this.expenseFacade.getExpenseList(this.segment, this.skip, limit);
  }

  deleteExpense(expenseId: string): void {
    this.expenseFacade.deleteExpense(expenseId);
  }

  search({ detail: { value } }): void {
    this.expenseFacade.resetExpenseList();
    if (value) {
      this.expenseFacade.searchExpense('b8fa471e-64c1-4c82-abe7-7b851e655289', value  );
    } else {
      this.expenseFacade.getExpenseList(this.segment, this.skip, 10);
    }
  }
}
