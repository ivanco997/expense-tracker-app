import {
  Component,
  Input,
  ChangeDetectionStrategy,
  Output,
  EventEmitter,
  ViewChild,
} from '@angular/core';
import { IExpense } from '@expenseapp/models/expense.model';
import { HelperService } from '../../../core/services/helper.service';
import { IonItemSliding } from '@ionic/angular';

@Component({
  selector: 'app-expense-list',
  templateUrl: './expense-list.component.html',
  styleUrls: ['./expense-list.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class ExpenseListComponent {
  @ViewChild(IonItemSliding, { static: false }) itemSliding: IonItemSliding;

  @Input() listName: string;
  @Input() expenses: { result: Array<IExpense> };
  @Input() isLoaded: boolean;
  @Input() hasError: boolean;

  @Output() deleteExpenseEmitter: EventEmitter<string> = new EventEmitter();
  @Output() skip: EventEmitter<number> = new EventEmitter();
  public readonly limit = 10;

  constructor(private helperService: HelperService) {}

  public deleteExpense(expenseId: string): void {
    this.helperService.presentAlert('Delete', 'Are you sure you want to delete this expense?', () => {
      this.itemSliding.close();
      this.deleteExpenseEmitter.emit(expenseId);
    });
  }
}
