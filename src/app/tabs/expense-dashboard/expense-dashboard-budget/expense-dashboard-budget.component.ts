import { Component, Input, Output, EventEmitter } from '@angular/core';
import { IUser } from '@expenseapp/models/user.model';
import { IExpenseDashboard } from '@expenseapp/tabs/store/models/expense-dashboard.model';
import { HelperService } from '../../../core/services/helper.service';

@Component({
  selector: 'app-expense-dashboard-budget',
  templateUrl: './expense-dashboard-budget.component.html',
  styleUrls: ['./expense-dashboard-budget.component.scss'],
})
export class ExpenseDashboardBudgetComponent {
  @Input() user: IUser;
  @Input() dashboard: IExpenseDashboard;
  @Output() amount: EventEmitter<string> = new EventEmitter();

  constructor(private helperService: HelperService) { }

  public async presentAlertPrompt(): Promise<void> {
    return await this.helperService.presentInputAlert<string>('Monthly budget', 'amount', 'Please enter your monhlty budget', this.amount);
  }
}
