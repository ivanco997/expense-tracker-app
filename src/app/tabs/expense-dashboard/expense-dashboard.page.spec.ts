import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { ExpenseDashboardPage } from './expense-dashboard.page';

describe('Tab2Page', () => {
  let component: ExpenseDashboardPage;
  let fixture: ComponentFixture<ExpenseDashboardPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ExpenseDashboardPage],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(ExpenseDashboardPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
