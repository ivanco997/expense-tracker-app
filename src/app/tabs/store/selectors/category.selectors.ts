import { ICategoryListState } from '../reducers/category.reducers';

export const getCategoriesList = (state: ICategoryListState) =>
  state.categories;
export const getCategoriesListLoaded = (state: ICategoryListState) =>
  state.isLoaded;
