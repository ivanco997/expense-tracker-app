export interface IExpenseCategory {
  id: string;
  name: string;
}
