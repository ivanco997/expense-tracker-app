export interface IExpenseDashboardCategory {
    name: string;
    amount: number;
    count: number;
}
export interface IExpenseDashboard {
    categories: IExpenseDashboardCategory[];
    totalMonthlyAmount: number;
}