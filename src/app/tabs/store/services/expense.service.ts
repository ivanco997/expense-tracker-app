import { Injectable } from '@angular/core';
import { HttpClient, HttpParams } from '@angular/common/http';
import { Observable } from 'rxjs';
import { IExpense, IExpenseResponse } from '@expenseapp/models/expense.model';
import { environment } from '../../../../environments/environment';

@Injectable({
  providedIn: 'root'
})
export class ExpenseService {
  constructor(private http: HttpClient) { }

  public addExpense(expense: IExpense, userId: string): Observable<IExpense> {
    const params = {
      params: new HttpParams().set('userId', userId)
    };
    return this.http.post<IExpense>(
      `${environment.API_URL}expense`,
      expense,
      params
    );
  }

  public getExpenses(
    userId: string,
    skip: number,
    limit: number,
    date?: string
  ): Observable<IExpenseResponse> {
    const params = {
      params: new HttpParams()
        .set('date', date)
        .set('userId', userId)
        .set('skip', skip.toString())
        .set('limit', limit.toString())
    };
    return this.http.get<IExpenseResponse>(
      `${environment.API_URL}expense`,
      params
    );
  }

  public deleteExpense(expenseId: string): Observable<IExpense> {
    const params = {
      params: new HttpParams().set('expenseId', expenseId)
    };
    return this.http.delete<IExpense>(`${environment.API_URL}expense`, params);
  }

  public searchExpenses(userId: string, searchTerm: string): Observable<IExpenseResponse> {
    const params = {
      params: new HttpParams().set('userId', userId).set('searchTerm', searchTerm)
    };
    return this.http.get<IExpenseResponse>(`${environment.API_URL}expense/search`, params);
  }
}
