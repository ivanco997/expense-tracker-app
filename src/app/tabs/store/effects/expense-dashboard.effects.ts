import { Actions, createEffect, ofType } from '@ngrx/effects';
import { Injectable } from '@angular/core';
import { switchMap, map, catchError } from 'rxjs/operators';
import { ExpenseDashboardService } from '../services/expense-dashboard.service';
import {
  getExpenseDashboardData,
  getExpenseDashboardDataSuccess,
  getExpenseDashboardDataFailed,
} from '../actions/expense-dashboard.actions';
import { UserService } from '@expenseapp/core/services/user.service';

@Injectable()
export class ExpenseDashboardEffects {
  constructor(
    private actions$: Actions,
    private expenseDashboardService: ExpenseDashboardService,
    private userService: UserService
  ) { }

  getDashboardData$ = createEffect(() =>
    this.actions$.pipe(
      ofType(getExpenseDashboardData),
      switchMap(({ userId, date }) =>
        this.expenseDashboardService.getExpenseDashboard(userId, date).pipe(
          map(dashboard => getExpenseDashboardDataSuccess({ dashboard })),
          catchError(error => [getExpenseDashboardDataFailed(error)])
        )
      )
    )
  )
}