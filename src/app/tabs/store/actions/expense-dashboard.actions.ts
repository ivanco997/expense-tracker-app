import { createAction, props } from '@ngrx/store'
import { IExpenseDashboard } from '../models/expense-dashboard.model'
import { IUser } from '@expenseapp/models/user.model'
import { actionType } from './type'

export const getExpenseDashboardData = createAction(
    actionType('[ExpenseDashboard] Load data'),
    props<{ userId: string, date: string }>()
)

export const getExpenseDashboardDataSuccess = createAction(
    actionType('[ExpenseDashboard] Load data success'),
    props<{ dashboard: IExpenseDashboard }>()
)

export const getExpenseDashboardDataFailed = createAction(
    actionType('[ExpenseDashboard] Load data failed'),
    (error: Error) => ({ error })
)
