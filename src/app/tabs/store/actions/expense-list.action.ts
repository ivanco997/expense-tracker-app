import { createAction, props } from '@ngrx/store';
import { IExpense, IExpenseResponse } from '@expenseapp/models/expense.model';
import { actionType } from './type';

export const loadExpenseList = createAction(
  '[ExpenseHistoryTab] Load expense',
  props<{ date: string; userId: string, skip: number; limit: number; }>()
);

export const loadExpenseListSuccess = createAction(
  actionType('[ExpenseHistoryTab] Load expense successfully'),
  props<{ expenses: IExpenseResponse }>()
);

export const loadExpenseListFailiure = createAction(
  actionType('[ExpenseHistoryTab] Load expense failed'),
  (error: Error) => ({ error })
);

export const deleteExpense = createAction(
  actionType('[ExpenseHistoryTab] Delete expense'),
  props<{ expenseId: string }>()
);

export const deleteExpenseSuccess = createAction(
  actionType('[ExpenseHistoryTab] Delete expense success'),
  props<{ expense: IExpense }>()
);

export const deleteExpenseFailure = createAction(
  actionType('[ExpenseHistoryTab] Delete expense failed'),
  (error: Error) => ({ error })
);

export const resetExpenseList = createAction(
  actionType('[ExpenseHistoryTab] Reset expense list')
)

export const searchExpenses = createAction(
  actionType('[ExpenseHistoryTab] Search expenses'), props<{ userId: string; searchTerm: string; }>()
)
export const searchExpensesSuccess = createAction(
  actionType('[ExpenseHistoryTab] Search expenses success'), props<{ expenses: IExpenseResponse }>()
)