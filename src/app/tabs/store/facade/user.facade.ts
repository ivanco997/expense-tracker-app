import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { IUser } from '../../../models/user.model';
import { Store, select } from '@ngrx/store';
import { IState } from '../reducers';
import { userSelector } from '../../../login/store/selectors';
import { updateUserAmout, getUserData } from '../../../login/store/actions';
import { UserService } from '../../../core/services/user.service';

@Injectable()
export class UserFacade {
    user$: Observable<IUser> = this.store.pipe(select(userSelector));

    constructor(private store: Store<IState>, private userService: UserService) { }

    public updateUserAmount(amount: number): void {
        this.store.dispatch(updateUserAmout({ userId: this.userService.userId, amount }));
    }

    public getUserData(): void {
        this.store.dispatch(getUserData({ userId: this.userService.userId }))
    }

    register(user): void {}
}