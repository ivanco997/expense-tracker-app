import { IExpenseResponse, IExpense } from '../../../models/expense.model';
import { createReducer, on, Action } from '@ngrx/store';
import {
  loadExpenseList,
  loadExpenseListSuccess,
  loadExpenseListFailiure,
  deleteExpenseSuccess,
  deleteExpenseFailure,
  resetExpenseList,
  searchExpensesSuccess,
} from '../actions/expense-list.action';

export interface IExpenseListState {
  expenses: IExpenseResponse;
  isLoaded?: boolean;
  error?: boolean;
}

const initialExpenseListState: IExpenseListState = {
  expenses: {
    result: [],
    total: 0,
  },
  isLoaded: false,
  error: false
};

const reducer = createReducer<IExpenseListState>(
  initialExpenseListState,
  on(loadExpenseList, (state) => ({ ...state })),
  on(loadExpenseListSuccess, (state, { expenses }) => ({
    ...state,
    expenses: {
      result: [...state.expenses.result, ...expenses.result],
      total: expenses.total,
    },
    isLoaded: true,
    error: false
  })),
  on(loadExpenseListFailiure, (state) => ({
    ...state,
    isLoaded: false,
    error: true
  })),
  on(resetExpenseList, (state) => ({
    ...state,
    expenses: {
      result: [],
      total: 0,
    },
    isLoaded: false,
  })),
  on(deleteExpenseSuccess, (state, { expense }) => ({
    ...state,
    expenses: {
      ...state.expenses,
      result: state.expenses.result.filter((x) => x.id !== expense.id),
      total:
        state.expenses.result
          .map((y) => y.amount)
          .reduce((acc, current) => acc + current) - expense.amount,
    },
  })),
  on(deleteExpenseFailure, () => initialExpenseListState),
  on(searchExpensesSuccess, (state, { expenses }) => ({
    ...state,
    expenses: {
      result: [...state.expenses.result, ...expenses.result],
      total: expenses.total,
    },
    isLoaded: true,
    error: false
  }))
);

export function expenseListReducer(state: IExpenseListState, action: Action) {
  return reducer(state, action);
}
