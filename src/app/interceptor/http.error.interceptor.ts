import { HttpEvent, HttpInterceptor, HttpHandler, HttpRequest, HttpErrorResponse } from '@angular/common/http';
import { Observable, throwError } from 'rxjs';
import { catchError } from 'rxjs/operators';
import { HelperService, ToastColorEnum } from '@expenseapp/core/services/helper.service';

export class HttpErrorInterceptor implements HttpInterceptor {
  constructor(private helperService: HelperService) {}

  intercept(request: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
    return next.handle(request).pipe(
      catchError((errorResponse: HttpErrorResponse) => {
        const errorMessage: string =
          errorResponse.error instanceof ErrorEvent
            ? `Error: ${errorResponse.error.message}`
            : `Error Code: ${errorResponse.status}\nMessage: ${errorResponse.error.message || errorResponse.message}`;

        this.helperService.presentToast(errorMessage, ToastColorEnum.DANGER);

        return throwError(errorMessage);
      })
    );
  }
}
