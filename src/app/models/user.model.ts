export interface ICredentials {
  username: string;
  password: string;
}

export interface IUser {
  id: string;
  username: string;
  fullName: string;
  amount: number;
  image: string;
}

export interface ILoginResponse {
  user: IUser;
  expiresIn: string;
  accessToken: string;
}
